#!/usr/bin/env bash

echo "restarting keepalived...";
cat /etc/keepalived.conf;
#TODO: this should be in a loop that tries to kill until it is dead
if pidof keepalived;then
    echo "trying to kill pid: $(pidof keepalived)";
    kill $(pidof keepalived);
fi
/usr/sbin/keepalived -d -f /etc/keepalived.conf ;
echo "running with pid: $(pidof keepalived)";

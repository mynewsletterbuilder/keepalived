#!/bin/sh
service rsyslog start;
export MY_IP=$(hostname -i | awk '{ print $1 }');
echo "setting up service: ${SERVICE}";
consul-template -consul=${CONSUL_URL} -template="/root/keepalived.conf.tmpl:/etc/keepalived.conf:/bin/sh restart_keepalived.sh";
